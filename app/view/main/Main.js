/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting causes an instance of this class to be created and
 * added to the Viewport container.
 *
 * TODO - Replace the content of this view to suit the needs of your application.
 */
Ext.define('Pratikum2.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',

    requires: [
        'Ext.MessageBox',

        'Pratikum2.view.main.MainController',
        'Pratikum2.view.main.MainModel',
        'Pratikum2.view.main.List'
    ],

    controller: 'main',
    viewModel: 'main',

    defaults: {
        tab: {
            iconAlign: 'top'
        },
        styleHtmlContent: true
    },

    tabBarPosition: 'bottom',

    items: [
        
            {
            title: 'Home',
            iconCls: 'x-fa fa-home',
            layout: 'fit',
            // The following grid shares a store with the classic version's grid as well!
            items: [
                {
                xtype: 'toolbar',
                docked: 'bottom',
                scrollable: {
                    y: false
                 },
                items: [
                {
                xtype: 'checkboxfield',
                label: 'show',
                flex: 1,
                reference: 'show'
            }, 
            {
                xtype:'spacer'
            },
                { 
                    xtype: 'button',
                    text:'show alert',
                    ui: 'action',
                    handler : 'onBtnclick',
                    bind: {
                         hidden: '{!show.checked}'
                    }
                }] 
            },

            {  
                xtype: 'mainlist'
            }]
        },{
            title: 'Users',
            iconCls: 'x-fa fa-user',
            bind: {
                html: '{loremIpsum}'
            }
        },{
            title: 'Groups',
            iconCls: 'x-fa fa-users',
            bind: {
                html: '{loremIpsum}'
            }
        },{
            title: 'Settings',
            iconCls: 'x-fa fa-cog',
            bind: {
                html: '{loremIpsum}'
            }
        }
    ]
});
