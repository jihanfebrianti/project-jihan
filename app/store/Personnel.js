Ext.define('Pratikum2.store.Personnel', {
    extend: 'Ext.data.Store',

    alias: 'store.personnel',

    fields: [
        'name', 'email', 'phone', 'tahun'
    ],

    data: { items: [
        { name: 'Jean Luc', email: "jeanluc.picard@enterprise.com", phone: "555-111-1111",  tahun: "1994"},
        { name: 'Worf',     email: "worf.moghsson@enterprise.com",  phone: "555-222-2222",  tahun: "1996" },
        { name: 'Deanna',   email: "deanna.troi@enterprise.com",    phone: "555-333-3333",  tahun: "1997" },
        { name: 'Data',     email: "mr.data@enterprise.com",        phone: "555-444-4444",  tahun: "1999" },
        { name: 'JihanFebrianti', email: "mr.data@enterprise.com", phone: "555-444-4444",   tahun: "2000" }
    ]},

    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
});
